// declare a new module called 'myApp', and make it require the `ng-admin` module as a dependency
var myApp = angular.module('myApp', ['ng-admin']);
// declare a function to run when the module bootstraps (during the 'config' phase)
myApp.config(['NgAdminConfigurationProvider', function (nga) {
    // create an admin application
    var admin = nga.application('Admin')
	.baseApiUrl('http://reporter.dev/app_dev.php/');

    var customer = nga.entity('customers');
    customer.listView().fields([
        nga.field('name'),
        nga.field('address'),
        nga.field('phone')
    ])
    .listActions(['show', 'edit', 'delete']);
    customer.showView().fields(customer.listView().fields());
    customer.creationView().fields(customer.listView().fields());
    customer.editionView().fields(customer.listView().fields());

    var task = nga.entity('tasks');
    task.listView().fields([
        nga.field('name').isDetailLink(true),
        nga.field('position')
    ])
    .listActions(['show', 'edit', 'delete']);
    task.showView().fields(task.listView().fields());
    task.creationView().fields(task.listView().fields());
    task.editionView().fields(task.creationView().fields());

    var company = nga.entity('companies');
    company.listView().fields([
        nga.field('name'),
        nga.field('address'),
        nga.field('phone')
    ])
    .listActions(['show', 'edit']);
    company.showView().fields(company.listView().fields());
    company.editionView().fields(company.listView().fields());

    admin
        .addEntity(customer)
        .addEntity(task)
        .addEntity(company)
    ;

    nga.configure(admin);
}]);
